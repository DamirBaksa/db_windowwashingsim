﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject ControlPanel;
    public GameObject controlText;
    public Text activationText;
    private PlayerControl playerControl;

    public bool canMove = false;
    public bool canStart = false;
    public bool canStop = false;
    public float vertSpeed;
    public float horSpeed;

    // Use this for initialization
    void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update ()
    {
        MoveCheck();
        if (canStart && Input.GetKeyDown(KeyCode.E))
        {
            canMove = true;
            activationText.text = "Press E to DeActivate";
            PlatFormMovement();
            canStart = false;
            Debug.Log("Platform Movement Enabled");
        }
        else if (!canStart && Input.GetKeyDown(KeyCode.E))
        {
            canMove = false;
            activationText.text = "Press E to Activate";
            PlayerMovement();
            canStart = true;
            Debug.Log("Platform Movement Disabled");
        }
    }

    public void MoveCheck()
    {
        if (canMove)
        {
            //Basic platform Movement
            Vector3 tDir = Vector3.zero;
            if (Input.GetKey(KeyCode.W))
            {
                transform.Translate(Vector3.up * vertSpeed);
            }
            else if (Input.GetKey(KeyCode.S))
            {
                transform.Translate(-Vector3.up * vertSpeed);
            }
            if (Input.GetKey(KeyCode.D))
            {
                transform.Translate(Vector3.forward * horSpeed);
            }
            else if (Input.GetKey(KeyCode.A))
            {
                transform.Translate(-Vector3.forward * horSpeed);
            }
        }
    }

    private void PlatFormMovement() // Call to incorporate platform movement 
    {
        controlText.SetActive(true);
        // Setting player movement to match platform movement
        Player.GetComponent<PlayerControl>().canMove = false;
         // enabling Platform movement
    }

    private void PlayerMovement() // Call to bring back regular player movement
    {
        controlText.SetActive(false);
        // Setting player movement to regular movement
        Player.GetComponent<PlayerControl>().canMove = true;
         // disabling Platform movement
    }
    
    //Function to activate platform accesibility
    public void ActivatePlatform()
    {
        canStart = true;
        activationText.text = "Press E to Activate";
        ControlPanel.SetActive(true);
    }
    //Function to deactivate platform accesibilty
    public void DeactivatePlatform()
    {
        canStart = false;
        ControlPanel.SetActive(false);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ActivatePlatform();
            other.gameObject.transform.parent = transform; // setting playerobj as child to platform to inherent platform movement
            Debug.Log("CanActivate");
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.transform.parent = other.transform; // returning player obj independant inheritence
            DeactivatePlatform();
            other.gameObject.GetComponent<Rigidbody>().useGravity = true;
        }
    }
}
