﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RagMotion : MonoBehaviour {

    public Transform rotPos;
    public Slider dirtMeter;
    public Renderer ragRenderer;

    Vector3 startPos;

    public float maxDirtTime = 10;
    public float dirtTime;
    public float rotateTime;
    public float rotWidth;
    public float rotHeight;
    public float rotSpeed;

	// Use this for initialization
	void Start ()
    {
        ragRenderer = GetComponent<Renderer>();
        dirtMeter.value = DirtMeterCalculation();

        Vector3 startPos = rotPos.position;
        rotWidth = 0.2f;
        rotHeight = 0.2f;
        rotSpeed = 7;
	}
	
	// Update is called once per frame
	void Update ()
    {
        CleanRag();
        transform.position = rotPos.position;
        if (Input.GetMouseButton(0))
        {
            dirtTime += Time.deltaTime;
            CircMotion();
        }
        dirtMeter.value = DirtMeterCalculation();
    }

    public void CircMotion()
    {
        rotateTime += Time.deltaTime * rotSpeed;

        float x = rotPos.position.x + (Mathf.Cos(rotateTime) * rotWidth);
        float y = rotPos.position.y + (Mathf.Sin(rotateTime) * rotHeight);
        float z = rotPos.position.z;

        transform.localPosition = startPos + new Vector3(x, y, z);
    }

    public float DirtMeterCalculation()
    {
        return dirtTime / maxDirtTime;
    }

    public void CleanRag()
    {
        if (Input.GetKey(KeyCode.R))
        {
            ragRenderer.enabled = false;
            dirtTime -= Time.deltaTime;
        }
        else
        {
            ragRenderer.enabled = true;
        }
    }
}
